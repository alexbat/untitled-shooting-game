///Full Scale With Aspect Ratio

var base_width = 640;
var base_height = 480;
var aspect_ratio =  display_get_width() / display_get_height();

if (aspect_ratio > 1){
    //landscape
    var scaled_width = base_height * aspect_ratio;
    display_set_gui_size(scaled_width, base_height);
    view_wport[0] = scaled_width;
    view_hport[0] = base_height;
    surface_resize(application_surface, view_wport[0], view_hport[0]);
}
