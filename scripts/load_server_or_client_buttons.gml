///
///argument 0 = load server buttons or client buttons

global.typeOfButtons = argument0

switch (global.typeOfButtons){
    case SERVER:        
        global.button[0] = room_instance_add(rm_server_client_configuration, 224, 64, obj_server_or_client_buttons);
        global.button[1] = room_instance_add(rm_server_client_configuration, 224, 160, obj_server_or_client_buttons);
        room_goto(rm_server_client_configuration);
        show_debug_message("loading server buttons");
        break;
    case CLIENT:
        global.button[0] = room_instance_add(rm_server_client_configuration, 448, 64, obj_server_or_client_buttons);
        global.button[1] = room_instance_add(rm_server_client_configuration, 448, 160, obj_server_or_client_buttons);
        room_goto(rm_server_client_configuration);
        show_debug_message("loading client buttons");
        break;
}

