///Set up both joysticks position and associated inner/outer sprites

var left_joystick = instance_create(64, 416, obj_inner_joystick);

with (left_joystick){
  associated_outer_joystick = instance_create(64, 416, obj_outer_joystick);
}
