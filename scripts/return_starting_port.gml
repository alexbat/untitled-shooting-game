///return_starting_port()

//Return the port that was chosen by the user for the server/client
//65535 is the max number of ports available (for either TCP or UDP)
//Ports need to be a higher number since low numbers are usually taken.

switch (global.chosenPort){
    case 0:
      return 40000;
      break;
    case 1:
      return 40100;
      break;
    case 2:
      return 40200;
      break;
    case 3:
      return 40300;
      break;
    case 4:
      return 40400;
      break;
    case 5:
      return 40500;
      break;
    case 6:
      return 40600;
      break;
    case 7:
      return 40700;
      break;
    case 8:
      return 40800;
      break;
    case 9:
      return 50000;
      break;
}
