///speed_scale(actual_speed_value, scale)

//show_debug_message(string(argument0 div argument1) + "  " + string(argument1));

//If alarm is off and scale value has not reached max value, continue setting alarms
if (alarm[0] == -1 && argument1 != 1)
  alarm[0] = room_speed * ALARM_INCREASE_SPEED;
  
//Return the scaled value of the given speed  
return argument0 div argument1;

