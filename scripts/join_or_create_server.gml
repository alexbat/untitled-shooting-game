///
///argument 0 = type of server being created
///argument 1 = x position for new instance
///argument 2 = y position for new instance


if (instance_exists(obj_server_or_client_buttons)){
  with(obj_server_or_client_buttons){
      instance_destroy();}
}

switch (argument0){
    case LOCAL_SERVER:
        var inst = instance_create(argument1, argument2, obj_server); 
        instance_create(64, 96, obj_play_game);
        inst.alarm[0] = room_speed;
        inst.local_server = true;
        break;
    case NETWORK_SERVER:
        instance_create(argument1, argument2, obj_server);
        instance_create(64, 96, obj_play_game);
        break;     
    case LOCAL_CLIENT:
        instance_create(argument1, argument2, obj_client_local); 
        break; 
    case NETWORK_CLIENT:
        instance_create(argument1, argument2, obj_client_network); 
        break; 
}

