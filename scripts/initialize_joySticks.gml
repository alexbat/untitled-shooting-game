///Set up both joysticks position and associated inner/outer sprites

obj_player.left_joystick = instance_create(64, 416, obj_outer_joystick);

with (obj_player.left_joystick){
  associated_inner_joystick = instance_create(64, 416, obj_inner_joystick);
}
