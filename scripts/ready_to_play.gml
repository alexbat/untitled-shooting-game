///sprite_alpha()

//Returns whether there if at least one other person (other than self)
//has joined the server and therefore ready to play

if (instance_exists(obj_server)){
    if (ds_list_size(obj_server.client_socket_list) > 1)
      return true;
}

return false;
